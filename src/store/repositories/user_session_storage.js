export function setTokenToSessionStorage(token) {
    if (token) {
        window.sessionStorage.setItem('token', JSON.stringify(token));
    }
}

export function getTokenFromSessionStorage() {
    return JSON.parse(window.sessionStorage.getItem('token'));
}

export function removeTokenFromSessionStorage() {
    window.sessionStorage.removeItem('token');
}
