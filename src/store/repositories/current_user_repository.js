import {patch} from '../../server/fetch';
import {store} from "../store";

export function getCurrentUserId() {
    return store.getState().user.data.id;
}

export function getCurrentUser() {
    return store.getState().user.data;
}

export function isUserLoggedIn() {
    return !!store.getState().user && !!store.getState().user.data;
}
