import { call, put, takeLatest } from 'redux-saga/effects';
import {post} from "../../server/fetch";
import { setError, setToken } from '../slices/user_slice';

function getTokenByLoginInfo(username, password) {
    return new Promise((resolve, reject) => {
        post(`/auth/login`, {username, password})
            .then((data) => {
                const token = data.jwt_token;
                if (token) {
                    resolve(token);
                }
                reject({message: data.message});
            })
    });
}

function postUserOnServer(userData) {
    return new Promise((resolve, reject) => post('/auth/register', userData)
        .then(() => resolve())
        .catch((e) => reject(e)));
}

function* getToken(action) {
    try {
        const token = yield call(getTokenByLoginInfo, action.payload.username, action.payload.password);
        yield put(setToken(token));
    } catch (e) {
        alert(e);
        yield put(setError({ message: e.message }));
    }
}

function* postUser(action) {
    try {
        yield call(postUserOnServer, action.payload);
    } catch (e) {
        alert(e);
        yield put(setError({ message: e.message }));
    }
}

export function* logInUser() {
    yield takeLatest('user/logIn', getToken);
}

export function* registerUser() {
    yield takeLatest('user/registerUser', postUser);
}
