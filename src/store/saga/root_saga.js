import { all } from 'redux-saga/effects';
import {logInUser, registerUser} from './auth_saga';

export function* rootSaga() {
    yield all([
        logInUser(),
        registerUser()
    ]);
}
