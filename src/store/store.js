import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { rootSaga } from './saga/root_saga';
import { setTokenToSessionStorage } from "./repositories/user_session_storage";
import userReducer from './slices/user_slice';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    userReducer, composeWithDevTools(
        applyMiddleware(sagaMiddleware),
    ),
);

sagaMiddleware.run(rootSaga);

window.addEventListener('unload', () => {
    const token = store.getState().user && store.getState().user.token;
    if (token) {
        setTokenToSessionStorage(token);
    }
});

export default store;
