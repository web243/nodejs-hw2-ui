import { createSlice } from '@reduxjs/toolkit';
import {
    getTokenFromSessionStorage,
    removeTokenFromSessionStorage,
    setTokenToSessionStorage
} from "../repositories/user_session_storage";

export const userSlice = createSlice({
    name: 'user',
    initialState: { token: getTokenFromSessionStorage() },
    reducers: {
        logIn: () => {},
        logOut: (state) => {
            state.token = null;
            removeTokenFromSessionStorage();
        },
        registerUser: () => {},
        setToken: (state, action) => {
            state.token = action.payload;
            setTokenToSessionStorage(action.payload);
        },
        setError: (state, action) => {
            state.error = action.payload;
        },
    },
});

export const {
    logIn, logOut, setToken, setError, registerUser
} = userSlice.actions;

export default userSlice.reducer;
