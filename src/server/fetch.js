import { SERVER_URL } from './url';

function getUrl(uri) {
    return `${SERVER_URL}${uri}`;
}

export function get(uri, encode = true) {
    let url = getUrl(uri);
    if (encode) {
        url = encodeURI(url);
    }
    return fetch(url);
}

export function getJson(uri, encode = true) {
    let url = getUrl(uri);
    if (encode) {
        url = encodeURI(url);
    }
    return fetch(url)
        .then((response) => response.json());
}

export function patch(uri, body) {
    const url = getUrl(uri);
    return fetch(url, {
        method: 'PATCH',
        body: JSON.stringify(body),
        headers: { 'Content-type': 'application/json; charset=UTF-8' },
    });
}

export function post(uri, body) {
    const url = getUrl(uri);
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: { 'Content-type': 'application/json' },
    });
}
