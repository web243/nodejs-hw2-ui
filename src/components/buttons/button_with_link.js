import {Link} from "react-router-dom";
import Button from "./button";
import React from "react";

function ButtonWithLink(props) {
    return <Link to={props.path}>
        <Button {...props}/>
    </Link>;
}

export default ButtonWithLink;
