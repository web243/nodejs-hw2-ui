import ButtonWithLink from "../button_with_link";
import React from "react";

function SettingsButton() {
    return <ButtonWithLink path="/settings" text="settings" className="button-dark-filled"/>
}

export default SettingsButton;
