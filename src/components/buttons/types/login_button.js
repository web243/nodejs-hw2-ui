import ButtonWithLink from "../button_with_link";
import React from "react";

function LoginButton() {
    return <ButtonWithLink path="/login" text="log in" className="button-dark-unfilled"/>
}

export default LoginButton;
