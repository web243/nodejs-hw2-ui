import ButtonWithLink from "../button_with_link";
import React from "react";

function RegisterButton() {
    return <ButtonWithLink path="/register" text="sign up" className="button-dark-filled"/>
}

export default RegisterButton;
