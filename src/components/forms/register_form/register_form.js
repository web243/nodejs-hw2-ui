import TextInput from "../../text_input/text_input";
import React from "react";
import Button from "../../buttons/button";
import {useDispatch} from "react-redux";
import {useFormik} from "formik";
import * as Yup from 'yup';
import {registerUser} from '../../../store/slices/user_slice';

const Schema = Yup.object().shape({
    password: Yup
        .string()
        .min(6, 'Password length should be at least 6 characters'),
    confirmPassword: Yup.string().when("password", {
        is: val => (!!(val && val.length > 0)),
        then: Yup.string().oneOf(
            [Yup.ref("password")],
            "Passwords do not match"
        )
    })
});

function RegisterForm(props) {
    const {values, errors, handleChange, handleSubmit} = props.formik;
    return (<form className="editor-form" onSubmit={handleSubmit}>
        <TextInput label="Username"
                   name="username"
                   type="text"
                   onChange={handleChange}
                   value={values.username}
                   max={50}
                   required={true}/>
        <TextInput label="Password"
                   name="password"
                   type="password"
                   onChange={handleChange}
                   value={values.password}
                   min={6}
                   required={true}/>
        <TextInput label="Confirm password"
                   name="confirmPassword"
                   type="password"
                   onChange={handleChange}
                   value={values.confirmPassword}
                   required={true}/>
        <p className="validation-error">{errors.password}</p>
        <p className="validation-error">{errors.confirmPassword}</p>
        <Button type="submit" text="submit" className="button-light-filled"/>
    </form>);
}

function RegisterFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(registerUser(values));
        },
    });
    return <RegisterForm formik={formik} />;
}

export default RegisterFormWithFormik;
