import TextInput from "../../text_input/text_input";
import React from "react";
import Button from "../../buttons/button";
import './editor_form.css';

function EditorForm() {
    return <form className="editor-form">
        <TextInput label="Text" className="textarea" required={true}/>
        <div className="editor-form-buttons-container">
            <Button text="cancel" className="button-dark-unfilled"/>
            <Button type="submit" text="save" className="button-light-filled"/>
        </div>

    </form>;
}

export default EditorForm;
