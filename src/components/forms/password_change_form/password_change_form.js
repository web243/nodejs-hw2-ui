import TextInput from "../../text_input/text_input";
import React from "react";
import Button from "../../buttons/button";


function PasswordChangeForm() {
    return <form className="editor-form">
        <TextInput label="Old password" type="password" required={true}/>
        <TextInput label="New password" type="password" required={true}/>
        <Button type="submit" text="save" className="button-light-filled"/>
    </form>;
}

export default PasswordChangeForm;
