import TextInput from "../../text_input/text_input";
import React from "react";
import Button from "../../buttons/button";
import {useDispatch} from "react-redux";
import {useFormik} from "formik";
import {registerUser} from "../../../store/slices/user_slice";

function LoginForm() {
    return <form className="editor-form">
        <TextInput label="Username" type="text" required={true}/>
        <TextInput label="Password" type="text" required={true}/>
        <Button type="submit" text="submit" className="button-light-filled"/>
    </form>;
}

function LoginFormWithFormik() {
    const dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Schema,
        onSubmit: (values) => {
            dispatch(registerUser(values));
        },
    });
    return <RegisterForm formik={formik} />;
}

export default LoginForm;
