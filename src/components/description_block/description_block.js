import React from "react";

function DescriptionBlock() {
    return <p className="short-description">Simple tool for creating checklists and managing notes</p>;
}

export default DescriptionBlock;
