import React from "react";
import RegisterForm from "../../forms/register_form/register_form";

function RegisterPage() {
    return <main>
        <RegisterForm />
    </main>;
}

export default RegisterPage;
