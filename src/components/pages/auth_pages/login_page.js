import React from "react";
import LoginForm from "../../forms/login_form/login_form";

function LoginPage() {
    return <main>
        <LoginForm />
    </main>;
}

export default LoginPage;
