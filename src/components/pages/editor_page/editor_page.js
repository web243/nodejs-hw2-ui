import React from "react";
import EditorForm from "../../forms/editor_form/editor_form";

function EditorPage(props) {
    return <main>
        <EditorForm />
    </main>;
}

export default EditorPage;
