import React from "react";
import PasswordChangeForm from "../../forms/password_change_form/password_change_form";

function SettingsPage(props) {
    return <main>
        <PasswordChangeForm />
    </main>;
}

export default SettingsPage;
