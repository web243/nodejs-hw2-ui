import Button from "../../buttons/button";
import React from "react";
import Note from "../../note/note";
import './user_profile.css';

function UserProfilePage() {
    return <main className="user-profile-container">
        <Button text="add note" className="button-light-filled"/>
        <Note text="Lorem Ipsum"/>
    </main>

}

export default UserProfilePage;
