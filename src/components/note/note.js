import React from "react";
import Button from "../buttons/button";
import './note.css';

function Note(props) {
    return <div className="note-container">
        <label>
            <input type="checkbox"/>
            {props.text}
        </label>
        <div className="buttons-container">
            <Button text="edit" className="button-link"/>
            <Button text="delete" className="button-link"/>
        </div>
    </div>;
}

export default Note;
