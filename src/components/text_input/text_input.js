import React from "react";
import './text_input.css';

function TextInput(props) {
    return <div className="text-input-container">
        <label className="caption-text">
            {`${props.label}:`}
        </label>
        {props.className === 'textarea' ?
            <textarea
                onChange={props.onChange ? props.onChange : null}
                name={props.name ? props.name : props.label}
                required={props.required}/> :
            <input type={props.type ? props.type : 'text'}
                   onChange={props.onChange ? props.onChange : null}
                   name={props.name ? props.name : props.label}
                   required={props.required}/>}
    </div>;
}

export default TextInput;
