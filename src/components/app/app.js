import React from "react";
import Header from './../header/header';
import {getRoutes} from "../../routes";
import {Switch} from "react-router-dom";

function App() {
  return (
    <React.Fragment>
        <Header />
        <Switch>
            {getRoutes()}
        </Switch>
    </React.Fragment>
  );
}

export default App;
