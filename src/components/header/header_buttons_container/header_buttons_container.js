import React from "react";
import './header_buttons_container.css';
import RegisterButton from "../../buttons/types/register_button";
import LoginButton from "../../buttons/types/login_button";

function HeaderButtonsContainer(props) {
    return <div className="header-buttons-container">
        <RegisterButton />
        <LoginButton />
    </div>;
}

export default HeaderButtonsContainer;
