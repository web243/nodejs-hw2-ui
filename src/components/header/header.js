import HeaderButtonsContainer from './header_buttons_container/header_buttons_container';
import React from "react";
import './header.css';
import {Link} from "react-router-dom";

function Header() {
    return <header>
        <Link to="/">
            <h1>Notes</h1>
        </Link>
        <HeaderButtonsContainer />
    </header>
}

export default Header;
