import HomePage from "./components/pages/home_page/home_page";
import LoginPage from "./components/pages/auth_pages/login_page";
import EditorPage from "./components/pages/editor_page/editor_page";
import UserProfilePage from "./components/pages/user_profile/user_profile";
import SettingsPage from "./components/pages/settings_page/settings_page";
import RegisterPage from "./components/pages/auth_pages/register_page";
import React from "react";
import {Route} from "react-router-dom";

export const ROUTES_WITH_COMPONENTS = {
    '/': HomePage,
    '/login': LoginPage,
    '/register': RegisterPage,
    '/editor': EditorPage,
    '/profile': UserProfilePage,
    '/settings': SettingsPage
};

export function getRoutes() {
    const routes = [];
    Object.entries(ROUTES_WITH_COMPONENTS).forEach(([path, component]) => {
        const route = (
            <Route key={path} exact={path === '/'} path={path} component={component} />
        );
        routes.push(route);
    });
    return routes;
}

